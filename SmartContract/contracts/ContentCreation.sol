// SPDX-License-Identifier: MIT
pragma solidity ^0.5.0;
// import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC20/ERC20.sol";
// import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
contract ContentCreation is ERC20 {
    string public name = "ContentCreationToken";
    string public description = "token for content creation";
    string public symbol = "CCT";
    uint8 public tokenDecimals = 18;
    uint public contentId = 0;
    address contractOwner;
    struct Content{
        uint contentID;
        string content;
        address contentOwner;
        uint contentLike;
    }

    mapping(address => bool) public hasUploadedContent;
    mapping(address => uint) public tokenEarned;
    // mapping (uint => Content) public likeOfTheParticularPost;
    mapping (uint => mapping (address => bool)) public hasLikedThePost;
    Content[] public  contentDetails;
    // mapping (address => uint[] ) public lostOfContentOfTheOwner;
    // constructor(uint initialToken) ERC20(tokenName,tokenSymbol) {
    //     contractOwner = msg.sender;
    //     _mint(msg.sender,initialToken * 10**uint(tokenDecimals));
    // }
    constructor() public {
        contractOwner = msg.sender;
        _mint(msg.sender,10000 * 10**uint(tokenDecimals));
    }
    function uploadContent(address contentOwner,string memory contentValue) public {
        contentId++;
         if(hasUploadedContent[contentOwner]){
            contentDetails.push(Content(contentId,contentValue,contentOwner,0));
         }else{
            hasUploadedContent[contentOwner] = true;
            _transfer(contractOwner,contentOwner,5);
            tokenEarned[contentOwner] = 5;
            contentDetails.push(Content(contentId,contentValue,contentOwner,0));
         }
    }
    function addLike(uint ID,address liker ) public payable {
        require(ID <= contentId, "Wrong id");
        require(hasLikedThePost[ID][liker]==false,"you have already liked the post");
        hasLikedThePost[ID][liker]=true;
        contentDetails[ID-1].contentLike+=1;
        _transfer(contractOwner,contentDetails[ID-1].contentOwner, 1);
        tokenEarned[contentDetails[ID-1].contentOwner]+=1;
    }
}
