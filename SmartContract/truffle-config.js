// const fs = require('fs');
// const HDWalletProvider = require('@truffle/hdwallet-provider');
// const infuraKey = "7a43e95ddf1e4e91a2ad33aeb8860a51";
// const privateKey = fs.readFileSync(".secret").toString().trim();
// const infuraURL = "https://sepolia.infura.io/v3/7a43e95ddf1e4e91a2ad33aeb8860a51";

// module.exports = {
//   networks: {
//     development: {
//       host: "host.docker.internal", // Localhost (default: none)
//       port: 7545, // Standard Ethereum port (default: none)
//       network_id: "*", // Any network (default: none)
//       // gas: 8000000
//     },
//     sepolia: {
//       provider: () => new HDWalletProvider(privateKey, infuraURL),
//       network_id: 11155111, // Sepolia's id
//       confirmations: 2, // # of confs to wait between deployments. (default: 0)
//       timeoutBlocks: 200, // # of blocks before deployment times out (minimum/default: 50)
//       skipDryRun: true // Skip dry run before migrations? (default: false for public nets)
//     },
//   },
//   compilers: {
//     solc: {
//       version: "^0.5.0",
//       optimizer: {
//         enabled: true,
//         runs: 200,
//       },
//       db: {
//         enabled: false,
//       },
//     },
//   },
// };




const fs = require('fs');
const HDWalletProvider = require('@truffle/hdwallet-provider');
const infuraKey = "d6000738699b4c66ac54b390d6a2c69e";
const privateKey = fs.readFileSync(".secret").toString().trim();
const infuraURL = "https://sepolia.infura.io/v3/7a43e95ddf1e4e91a2ad33aeb8860a51";

module.exports = {
  networks: {
    development: {
      host: "host.docker.internal", // Localhost (default: none)
      port: 7545, // Standard Ethereum port (default: none)
      network_id: "*", // Any network (default: none)
    },
    sepolia: {
      provider: () => new HDWalletProvider(privateKey, infuraURL),
      network_id: 11155111, // Sepolia's id
      confirmations: 2, // # of confs to wait between deployments. (default: 0)
      timeoutBlocks: 200, // # of blocks before deployment times out (minimum/default: 50)
      skipDryRun: true // Skip dry run before migrations? (default: false for public nets)
    },
  },
  compilers: {
    solc: {
      version: "^0.5.0",
      optimizer: {
        enabled: true,
        runs: 200,
      },
      db: {
        enabled: false,
      },
    },
  },
};
